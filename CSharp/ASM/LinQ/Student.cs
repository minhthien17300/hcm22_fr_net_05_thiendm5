﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ
{
    class Student
    {
        private string _name;
        private int _age;
        private int _idClass;

        public int IDClass
        {
            get { return _idClass; }
            set { _idClass = value; }
        }


        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Student(string name, int age, int idClass)
        {
            _name = name;
            _age = age;
            _idClass = idClass;
        }

    }
}
