﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ
{
    class Class
    {
        private int _idClass;
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int IDClass
        {
            get { return _idClass; }
            set { _idClass = value; }
        }

        public Class(int idClass, string name)
        {
            _idClass = idClass;
            _name = name;
        }

    }
}
