﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Class> classes = new List<Class>();
            Class class1 = new Class(1, "OOP");
            Class class2 = new Class(2, "C#");
            classes.Add(class1);
            classes.Add(class2);

            List<Student> students = new List<Student>();
            Student student1 = new Student("Le Van Let", 22, 1);
            Student student2 = new Student("Dao Thi Buoi", 22, 2);
            Student student3 = new Student("Phuc N'Go", 22, 1);
            Student student4 = new Student("Vo Binh Dinh", 22, 2);
            Student student5 = new Student("Hoang Minh Quang", 22, 2);
            students.Add(student1);
            students.Add(student2);
            students.Add(student3);
            students.Add(student4);
            students.Add(student5);

            var groupList = classes
                .GroupJoin(
                    students,
                    cl => cl.IDClass,
                    stu => stu.IDClass,
                    (cl, stu) => new
                    {
                        _className = cl.Name,
                        StudentList = stu
                    }
                );

            Console.WriteLine("StudentList Group by Class");
            foreach(var item in groupList)
            {
                Console.WriteLine($"Class: {item._className}");
                foreach(var student in item.StudentList)
                    Console.WriteLine($"{student.Name}, {student.Age}");
            }

            Console.WriteLine("The number of students in each class");
            foreach (var item in groupList)
            {
                var count = 0;
                foreach (var student in item.StudentList)
                    count++;
                Console.WriteLine($"Class: {item._className} has {count} students");
                
            }

            Console.WriteLine("Student string in each class");
            foreach (var item in groupList)
            {
                var text = "";
                foreach (var student in item.StudentList)
                    text = text + $"{student.Name} - {student.Age}, ";
                Console.WriteLine($"Class: {item._className}");
                Console.WriteLine($"Students: {text}");

            }

            Console.ReadKey();
        }
    }
}
