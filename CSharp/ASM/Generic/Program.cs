﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_5_Assignment
{
    internal class Program
    {
        public class GenericList<T>
        {
            private T[] list;

            public GenericList(T[] arr)
            {
                list = new T[arr.Length];
                for (int i = 0; i < arr.Length; i++)
                {
                    list[i] = arr[i];
                }
            }
            private void IncreaseArraySize()
            {
                var arr = new T[list.Length];
                list.CopyTo(arr, 0);
                list = new T[arr.Length + 1];
                arr.CopyTo(list, 0);
            }
            private void DecreaseArraySize()
            {
                var arr = new T[list.Length - 1];
                for(var i = 0; i < arr.Length; i++)
                {
                    arr[i] = list[i];
                }
                list = new T[arr.Length];
                arr.CopyTo(list, 0);
            }
            public T[] GetList()
            {
                return list;
            }

            public void Add<U>(U item) where U : T
            {
                IncreaseArraySize();
                list[list.Length - 1] = item;
            }
            public void Delete<U>(U item) where U: T
            {
                for(int i = 0; i < list.Length; i++)
                {
                    if (list[i].Equals(item))
                    {
                        if (i == list.Length - 1)
                        {
                            list[i] = default(U);
                        }
                        else
                        {
                            for (int j = i; j < list.Length - 1; j++)
                            {
                                list[j] = list[j + 1];
                            }
                        }
                        DecreaseArraySize();
                        i--;
                    }
                }
            }
            public T GetItemByIndex(int index)
            {
                if(index <0 || index >= list.Length)
                {
                    return default(T);
                }
                
                return list[index];
            }
            public void SetItemByIndex<U>(int index, U item) where U : T
            {
                if (index < 0 || index >= list.Length)
                {
                    Console.WriteLine("Nhap sai index");
                }
                else
                {
                    list[index] = item;
                }
            }
            public int GetCount()
            {
                return list.Length;
            }
        }
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3 };
            GenericList<int> genericList = new GenericList<int>(arr);
            var result1 = genericList.GetList();
            Console.WriteLine("Generic List 1:");
            foreach(var item in result1)
            {
                Console.WriteLine(item);
            }

            genericList.Add<int>(4);
            var result2 = genericList.GetList();
            Console.WriteLine("Generic List 2:");
            foreach (var item in result2)
            {
                Console.WriteLine(item);
            }
            

            genericList.Delete<int>(3);
            var result3 = genericList.GetList();
            Console.WriteLine("Generic List 3:");
            foreach (var item in result3)
            {
                Console.WriteLine(item);
            }

            var index = 1;
            Console.Write($"Item at Index {index}: ");
            var result4 = genericList.GetItemByIndex(index);
            Console.WriteLine(result4);

            genericList.SetItemByIndex<int>(index, 5);
            Console.Write($"Item at Index {index}: ");
            var result5 = genericList.GetItemByIndex(index);
            Console.WriteLine(result5);

            var result6 = genericList.GetCount();
            Console.WriteLine($"List Length: {result6}");

            Console.ReadKey();
        }
    }
}
