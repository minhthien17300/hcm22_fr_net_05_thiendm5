﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection
{
    class Movie
    {
        private int _id;
        private string _name;
        private string _director;

        public string Director
        {
            get { return _director; }
            set { _director = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public Movie(int id, string name, string director)
        {
            _id = id;
            _name = name;
            _director = director;
        }

    }
}
