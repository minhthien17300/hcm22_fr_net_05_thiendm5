﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection
{
    class Program
    {
        static void PrintMoviesList(List<Movie> movies)
        {
            foreach (var item in movies)
            {
                Console.WriteLine($"{item.ID}, {item.Name}, {item.Director}");
            }
        }

        static void Main(string[] args)
        {
            List<Movie> movies = new List<Movie>();
            Movie movie1 = new Movie(2, "Titanic", "Jame Cameron");
            Movie movie2 = new Movie(13, "Titanic2", "Jame Cameron");
            Movie movie3 = new Movie(7, "Movie4", "Jame Cameron");
            Movie movie4 = new Movie(27, "Titanic4", "Jame Cameron");
            
            movies.Add(movie1);
            movies.Add(movie2);
            movies.Add(movie3);
            movies.Add(movie4);
            Console.WriteLine("2a");
            PrintMoviesList(movies);

            movies.Remove(movie2);
            Console.WriteLine("2b");
            PrintMoviesList(movies);

            Movie movie5 = new Movie(34, "Titanic5", "Jame Cameron");
            movies.Insert(2, movie5);
            Console.WriteLine("2c");
            PrintMoviesList(movies);

            var sortedMovies = movies.OrderByDescending(movie => movie.ID).ToList();
            Console.WriteLine("2d");
            PrintMoviesList(sortedMovies);

            Console.WriteLine("2e");
            foreach(var item in movies)
            {
                if(item.Name == "Movie4")
                {
                    Console.WriteLine($"{item.ID}, {item.Name}, {item.Director}");
                }
            }

            Console.ReadKey();
        }
    }
}
