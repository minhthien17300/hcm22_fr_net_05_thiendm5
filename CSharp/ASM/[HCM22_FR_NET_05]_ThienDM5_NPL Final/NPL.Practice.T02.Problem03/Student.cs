﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student: IGraduate
    {
        private int _id;
        private string _name;
        private DateTime _startDate;
        private decimal _sqlMark;
        private decimal _csharpMark;
        private decimal _dsaMark;
        private decimal _gpa;
        private GraduateLevel _graduateLevel;

        public decimal DsaMark
        {
            get { return _dsaMark; }
            set { _dsaMark = value; }
        }


        public decimal CsharpMark
        {
            get { return _csharpMark; }
            set { _csharpMark = value; }
        }


        public decimal SqlMark
        {
            get { return _sqlMark; }
            set { _sqlMark = value; }
        }


        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }



        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public void Graduate()
        {
            //tính điểm GPA
            var gpa = (_sqlMark + _csharpMark + _dsaMark) / 3;
            //làm tròn 2 chữ số thập phân và gán giá trị cho field _gpa
            _gpa = Math.Round(gpa, 2);

            //xử lý xếp loại cho Student
            if (gpa < 5)
                _graduateLevel = GraduateLevel.Failed;
            else if (gpa < 7)
                _graduateLevel = GraduateLevel.Average;
            else if (gpa < 8)
                _graduateLevel = GraduateLevel.Good;
            else if (gpa < 9)
                _graduateLevel = GraduateLevel.VeryGood;
            else
                _graduateLevel = GraduateLevel.Excellent;
        }

        public string GetCertificate()
        {
            //xuất chuỗi string theo format đề
            var certificateSring = $"Name: {_name}, SqlMark: {_sqlMark}, CsharpMark: {_csharpMark}, DsaMark: {_dsaMark}, GPA: {_gpa}, GraduateLevel: {_graduateLevel}";
            return certificateSring;
        }
    }
}
