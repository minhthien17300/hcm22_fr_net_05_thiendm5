﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    class Program
    {
        static void Main(string[] args)
        {
            //test program
            //tạo student và gán dữ liệu mẫu
            Student student = new Student();
            student.Id = 1;
            student.Name = "Peter";
            student.StartDate = DateTime.Parse("23-04-2022");
            student.SqlMark = 10m;
            student.CsharpMark = 9.5m;
            student.DsaMark = 10m;

            //chạy method Graduate()
            student.Graduate();

            //chạy method GetCertificate()
            var certificate = student.GetCertificate();
            Console.WriteLine(certificate);

            Console.ReadKey();
        }
    }
}
