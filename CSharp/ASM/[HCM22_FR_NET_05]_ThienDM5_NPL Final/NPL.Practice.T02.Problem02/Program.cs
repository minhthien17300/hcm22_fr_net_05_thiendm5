﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {
        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            //xác định giá trị sumMax đầu tiên
            var maxSum = 0;
            for(var i = 0; i < subLength; i++)
            {
                maxSum += inputArray[i];
            }

            //Lặp trên inputArray bắt đầu từ index 1 cho tới vị trí cách index cuối "sunLength" đơn vị
            for(var i = 1; i <= inputArray.Length - subLength; i++)
            {
                //gán giá trị sum = 0 với mỗi lần lặp trong inputArray
                var sum = 0;
                //Lập trên subArray với subLength cho trước
                for(var j = i; j < i + subLength; j++)
                {
                    sum += inputArray[j];
                }

                //so sánh 2 giá trị sum và maxSum, nếu sum lớn hơn thì gán maxSum = sum
                if (sum > maxSum) maxSum = sum;
            }

            //trả về maxSum
            return maxSum;
        }
        static void Main(string[] args)
        {
            //test method
            int[] inputArray = { 1, 2, 5, -4, 3 };
            var subLength = 3;

            var resultMaxSum = FindMaxSubArray(inputArray, subLength);
            Console.WriteLine($"MaxSum of Subarray = {resultMaxSum}");

            Console.ReadKey();
        }
    }
}
