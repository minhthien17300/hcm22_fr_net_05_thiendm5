﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem01
{
    class Program
    {
        static public string GetArticleSummary(string content, int maxLength)
        {
            //lưu vị trí cuối của kí tự khoảng cách
            var lastSpaceIndex = 0;
            //lưu summary sau khi cắt, khởi tạo bằng chính content
            var shortSummary = content;

            //lặp tới khi nào tìm thấy vị trí của kí tự khoảng cách gần maxLength về bên trái nhất
            do
            {
                //lưu lại vị trí sau mỗi lần lặp
                lastSpaceIndex = shortSummary.LastIndexOf(" ");
                //cắt summary mỗi lần lặp
                shortSummary = shortSummary.Substring(0, lastSpaceIndex);
            }
            while (lastSpaceIndex > maxLength);

            //trả về summary đã cắt + "..."
            var resultSummary = $"{shortSummary}...";
            return resultSummary;
        }
        static void Main(string[] args)
        {
            //test method
            var content = "One of the world's biggest festivals hit the streets of London";
            var maxLength = 50;
            var resultSummary = GetArticleSummary(content, maxLength);

            Console.WriteLine(resultSummary);

            Console.ReadKey();
        }
    }
}
