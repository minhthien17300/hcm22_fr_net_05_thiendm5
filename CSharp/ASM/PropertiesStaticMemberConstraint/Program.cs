﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4_Assignment
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Employee employee = new Employee(1, "Le Van Let", 2000000, 1);
            Console.WriteLine("ID: " + employee.ID);
            Console.WriteLine("Name: " + employee.Name);
            Console.WriteLine("Salary: " + employee.Salary);
            Console.WriteLine("Role: " + employee.Role);

            Employee employee1 = new Employee(2, "Dao Thi Buoi", 2032323, 3);
            employee1.Salary = 73882312;

            Console.ReadKey();
        }
    }
}
