﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_4_Assignment
{
    internal class Employee
    {
        private int _id;
        private string _name;
        private int _salary;
        //role = 0 là người dùng phổ thông, = 1 là admin
        private int _role;
        private static int _count_Check = 0;
        private const int _charge = 100000; 

        
        private void IncreaseCount_Check()
        {
            _count_Check++;
        }
        private bool CheckRole(int role)
        {
            int[] roles = { 0, 1 };
            if (roles.Contains(role)) return true;
            else return false;
        }
        private void PrintWarning()
        {
            if(_count_Check > 40)
            {
                Console.WriteLine("IMPOSRTER DETECTED! YOU ARE UNDER ARRESTED");
                Console.WriteLine("Charge: " + _charge * (_count_Check / 40));
            }
        }

        public int ID
        {
            get 
            {
                IncreaseCount_Check();
                return _id; 
            }
            set 
            { 
                _id = value;
                IncreaseCount_Check();
            }
        }
        public string Name
        {
            get
            {
                IncreaseCount_Check();
                return _name;
            }
            set
            {
                _name = value;
                IncreaseCount_Check();
            }
        }
        public int Salary
        {
            get
            {
                IncreaseCount_Check();
                return _salary;
            }
            set
            {
                if (_role != 1) _count_Check += 40;
                else
                {
                    _salary = value;
                    IncreaseCount_Check();
                }

                PrintWarning();  
            }
        }
        public int Role
        {
            get
            {
                IncreaseCount_Check();
                return _role;
            }
            set
            {
                if (CheckRole(value))
                {
                    _role = value;
                    IncreaseCount_Check();
                }
                else
                {
                    _role = -1;
                }
            }
        }
        public Employee(int id, string name, int salary, int role)
        {
            _id = id;
            _name = name;
            _salary = salary;
            if (CheckRole(role))
            {
                _role = role;
                IncreaseCount_Check();
            }
            else
            {
                _role = -1;
            }
        }
    }
}
