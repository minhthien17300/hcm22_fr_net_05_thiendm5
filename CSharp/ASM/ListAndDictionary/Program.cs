﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndDictionary
{
    class Program
    {
        static void PrintListStudents(List<Student> students)
        {
            foreach(var item in students)
            {
                Console.WriteLine($"{item.StudentID}, {item.Name}, {item.Age}, {item.Class}");
            }
        }
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student(1, "Le Van Let", 22, "OOP");
            Student student2 = new Student(69, "Dao Thi Buoi", 23, "OOP");
            Student student3 = new Student(34, "Phuc N'Go", 19, "OOP");
            Student student4 = new Student(77, "Mai Thanh Cong", 27, "OOP");
            Student student5 = new Student(26, "Vo Binh Dinh", 20, "OOP");

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);
            students.Add(student4);
            students.Add(student5);
            Console.WriteLine("2");
            PrintListStudents(students);

            Console.WriteLine("3");
            if(students.Contains(student3))
                Console.WriteLine("Yes He/She is here!");
            else Console.WriteLine("No He/She is not here!");

            var sortedStudents = new List<Student>();
            sortedStudents.AddRange(students);
            sortedStudents.Sort((x, y) =>
            {
                int ret = x.StudentID.CompareTo(y.StudentID);
                return ret;
            });
            sortedStudents.Reverse();
            Console.WriteLine("4");
            Console.WriteLine("Not sort");
            PrintListStudents(students);
            Console.WriteLine("Sorted");
            PrintListStudents(sortedStudents);

            students.Remove(student2);
            Console.WriteLine("5");
            PrintListStudents(students);

            var listLenght = students.Count;
            Console.WriteLine("6");
            Console.WriteLine($"The number of students is: {listLenght}");

            Console.ReadKey();
        }
    }
}
