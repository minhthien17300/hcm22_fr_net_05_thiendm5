﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndDictionary
{
    class Student
    {
        private int _studentID;
        private string _name;
        private int _age;
        private string _class;

        public string Class
        {
            get { return _class; }
            set { _class = value; }
        }


        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int StudentID
        {
            get { return _studentID; }
            set { _studentID = value; }
        }
        public Student(int studentID, string name, int age, string _class)
        {
            _studentID = studentID;
            _name = name;
            _age = age;
            this._class = _class;
        }
    }
}
