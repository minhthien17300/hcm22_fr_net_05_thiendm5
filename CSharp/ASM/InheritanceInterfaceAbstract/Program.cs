﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance_Exercise
{
    public abstract class Payment
    {
        private string _paymentNo = "09127863725";
        public virtual string GetPaymentNo()
        {
            return _paymentNo;
        }
        
    }
    public interface IOrder
    {
        void PlaceOrder();
        void CancelOrder();
        void GetOrder();
    }

    public class PizzaOrder: Payment, IOrder
    {
        private int _price = 50000;
        private int _amount = 0;
        private bool _isOrdered = false;

        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public PizzaOrder(int amount)
        {
            _amount = amount;
        }

        public void PlaceOrder()
        {
            var paymentNo = GetPaymentNo();
            if(_amount == 0)
            {
                Console.WriteLine("Please input amount!");
            }
            else if(!_isOrdered)
            {
                _isOrdered = !_isOrdered;
                var totalBill = _amount * _price;
                Console.WriteLine($"PaymentNo: {paymentNo}");
                Console.WriteLine($"Bill: {totalBill}");
            }
            else
            {
                Console.WriteLine("You already place an order!");
            }
        }
        
        public void CancelOrder()
        {
            if (_isOrdered)
            {
                _isOrdered = !_isOrdered;
                Console.WriteLine("Cancel order successfully!");
            }
            else
            {
                Console.WriteLine("You haven't placed any order yet!");
            }
        }
        
        public void GetOrder()
        {
            var totalBill = _amount * _price;
            var paymentNo = GetPaymentNo();
            Console.WriteLine("Pizza Order Information:");
            Console.WriteLine($"Amount: {_amount}");
            Console.WriteLine($"Price: {_price}");
            Console.WriteLine($"Bill: {totalBill}");
            Console.WriteLine($"PaymentNo: {paymentNo}");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            PizzaOrder pizzaOrder = new PizzaOrder(0);
            pizzaOrder.PlaceOrder();

            pizzaOrder.Amount = 2;
            pizzaOrder.CancelOrder();

            pizzaOrder.PlaceOrder();
            pizzaOrder.PlaceOrder();

            pizzaOrder.GetOrder();

            pizzaOrder.CancelOrder();

            Console.ReadKey();
        }
    }
}
