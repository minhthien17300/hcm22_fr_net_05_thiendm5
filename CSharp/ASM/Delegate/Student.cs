﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    class Student
    {
        private int _studetnID;
        private string _name;
        private DateTime _birth;
        private bool _gender; //true is male, another is female

        public bool Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }


        public DateTime Birth
        {
            get { return _birth; }
            set { _birth = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int StudentID
        {
            get { return _studetnID; }
            set { _studetnID = value; }
        }

        //public Student(int studentID, string name, DateTime birth, bool gender)
        //{
        //    _studetnID = studentID;
        //    _name = name;
        //    _birth = birth;
        //    _gender = gender;
        //}

    }
}
