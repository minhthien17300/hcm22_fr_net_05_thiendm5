﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delegate;

delegate Student IOStudent(ref Student iOStudent);
namespace Delegate
{
    class Program
    {
        static Student student = new Student();
        static Student InputStudent(ref Student student)
        {
            Console.Write("Input StudentID: ");
            student.StudentID = int.Parse(Console.ReadLine());
            Console.Write("Input Name: ");
            student.Name = Console.ReadLine();
            Console.Write("Input Birth: ");
            student.Birth = DateTime.Parse(Console.ReadLine());
            Console.Write("Input Gender (use 1 for male and 0 for female): ");
            var gender = int.Parse(Console.ReadLine());
            if (gender == 1) student.Gender = true;
            else student.Gender = false;
            return student;
        }

        static Student PrintStudent(ref Student student)
        {
            var gender = "Female";
            if (student.Gender) gender = "Male";
            Console.WriteLine($"{student.StudentID}, {student.Name}, {student.Birth}, {gender}");
            return student;
        }
        static void Main(string[] args)
        {
            IOStudent inputStudent = new IOStudent(InputStudent);
            IOStudent printStudent = new IOStudent(PrintStudent);

            inputStudent(ref student);
            printStudent(ref student);
            Console.ReadKey();
        }
    }
}
