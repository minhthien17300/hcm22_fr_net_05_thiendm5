﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Shape
    {
        public int Width;
        public int Height;

        public virtual int Area(int width, int height)
        {
            return width + height;
        }
    }
}
