﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Rectangle: Shape
    {
        public override int Area(int width, int height)
        {
            return width * height;    
        }

        public void Size(int sameSize)
        {
            Width = Height = sameSize;
        }

        public void Size(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public Rectangle()
        {

        }
        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public static Rectangle operator +(Rectangle a, Rectangle b)
            => new Rectangle(a.Width + b.Width, a.Height + b.Height);

        public static Rectangle operator -(Rectangle a, Rectangle b)
            => new Rectangle(a.Width - b.Width, a.Height - b.Height);
    }
}
