﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rect = new Rectangle();

            var area = rect.Area(5, 3);
            Console.WriteLine($"Area of Rectangle: {area}");

            Rectangle rect1 = new Rectangle(3, 5);
            Rectangle rect2 = new Rectangle(1, 3);
            var resultRect1 = rect1 + rect2;
            Console.WriteLine($"+ Rect = ({resultRect1.Width}, {resultRect1.Height})");
            var resultRect2 = rect1 - rect2;
            Console.WriteLine($"- Rect = ({resultRect2.Width}, {resultRect2.Height})");

            Console.ReadKey();

        }
    }
}
